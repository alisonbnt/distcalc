FROM golang:1.13-alpine3.11

RUN mkdir /app

ADD . /app

WORKDIR /app

RUN go build -o main .

CMD ["/app/main", "--local", "--httpAddr=0.0.0.0:8000"]
