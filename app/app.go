package app

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"

	"issiata.com/dev/distcalc/arithmetic"
	"issiata.com/dev/distcalc/exprparse"
	"issiata.com/dev/distcalc/remote"
	"issiata.com/dev/distcalc/web"
)

type App struct {
	httpAddr          string
	gRPCArCentralAddr string
}

func NewApp(httpAddr string, gRPCArCentralAddr string) *App {
	return &App{
		httpAddr:          httpAddr,
		gRPCArCentralAddr: gRPCArCentralAddr,
	}
}

func (app *App) Up(asLocal bool) {
	if asLocal {
		fmt.Println("Local mode - gRPC layer disabled")
	}
	addr := app.httpAddr

	notPass := app.wireNotationPass()
	remoteArCentral := app.wireRemoteArithmeticCentral()
	ctx := app.wireArithmeticContext()
	var engine arithmetic.Engine
	if asLocal {
		engine = app.wireHardCodedEngine()
	} else {
		engine = app.wireArithmeticEngine(remoteArCentral)
	}
	ar := app.wireArithmetic(engine, ctx)
	parser := app.wireStrParser(ar, notPass)
	server := app.wireHTTPServer(addr, parser, ctx, notPass)

	go server.Run()
	if !asLocal {
		go func() {
			remoteUpErr := remoteArCentral.Up(app.gRPCArCentralAddr)
			if remoteUpErr != nil {
				log.Fatal("Error while starting gRPC server:", remoteUpErr)
			}
		}()
	}

	app.waitForInterrupt()
}

// func (app *App) Down() {}

func (app *App) wireHardCodedEngine() arithmetic.Engine {
	return &arithmetic.HardCodedEngine{}
}

func (app *App) wireNotationPass() *exprparse.NotationPass {
	return &exprparse.NotationPass{}
}

func (app *App) wireRemoteArithmeticCentral() *remote.ArithmeticCentral {
	return remote.NewDefaultArithmeticCentral()
}

func (app *App) wireArithmeticContext() *arithmetic.ArithmeticContext {
	return arithmetic.NewArithmeticContext()
}

func (app *App) wireArithmeticEngine(central *remote.ArithmeticCentral) arithmetic.Engine {
	return arithmetic.NewRemoteEngine(central)
}

func (app *App) wireArithmetic(eng arithmetic.Engine, ctx *arithmetic.ArithmeticContext) *arithmetic.Arithmetic {
	return arithmetic.NewArithmetic(eng, ctx)
}

func (app *App) wireStrParser(ar *arithmetic.Arithmetic, notPass *exprparse.NotationPass) exprparse.StrExprParser {
	return exprparse.NewMultipleNotationExprParser(notPass, ar)
}

func (app *App) wireHTTPServer(
	addr string,
	parser exprparse.StrExprParser,
	ctx *arithmetic.ArithmeticContext,
	notPub web.NotationPublisher,
) *web.HTTPServer {
	arService := web.NewWiredArithmeticService(parser)
	arCtx := web.NewWiredArithmeticContext(ctx)
	return web.NewHTTPServer(addr, notPub, arService, arCtx)
}

func (app *App) waitForInterrupt() {
	signalChannel := make(chan os.Signal, 1)
	signal.Notify(signalChannel, os.Interrupt, syscall.SIGTERM)
	<-signalChannel
}

func (app *App) StartService(service string, serviceAddr string) {
	daemon := remote.NewDefaultServiceDaemon(app.gRPCArCentralAddr, serviceAddr, service)
	go func() {
		upErr := daemon.Up(serviceAddr)
		if upErr != nil {
			log.Fatal("Error while starting service:", upErr)
		}
	}()

	app.waitForInterrupt()
}
