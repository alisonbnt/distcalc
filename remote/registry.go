package remote

import (
	"context"
	"fmt"
	"unicode/utf8"

	"google.golang.org/grpc"
)

type registry struct {
	maxUnavailabilities int
	services            map[rune]*arServiceClientCollection
}

func newRegistry() *registry {
	return &registry{
		// TODO: Remove magic number
		maxUnavailabilities: 3,
		services:            map[rune]*arServiceClientCollection{},
	}
}

func (reg *registry) Register(ctx context.Context, desc *ServiceDescription) (*RegistryResponse, error) {
	symbolStr := desc.GetSymbol()
	if len(symbolStr) > 1 {
		return nil, fmt.Errorf("Too many characters for symbol: %v", symbolStr)
	}
	symbol, _ := utf8.DecodeRuneInString(symbolStr)

	addr := desc.GetAddress()
	conn, err := grpc.Dial(addr, grpc.WithInsecure())
	if err != nil {
		return nil, err
	}

	if symbol != '+' && symbol != '-' && symbol != '*' && symbol != '/' {
		return nil, fmt.Errorf("Unknown operation %v", string(symbol))
	}

	client := NewArithmeticServiceClient(conn)
	wrapper := newArServiceClientWrapper(addr, client, reg.maxUnavailabilities)
	reg.registerServiceFor(symbol, addr, wrapper)
	return &RegistryResponse{
		Successful: true,
		Msg:        fmt.Sprintf("Successfully registered as %v operation", string(symbol)),
	}, nil
}

func (reg *registry) registerServiceFor(op rune, addr string, wrapper *arServClientWrapper) {
	if _, ok := reg.services[op]; !ok {
		reg.services[op] = newArServiceClientCollection()
	}

	coll := reg.services[op]
	coll.addService(addr, wrapper)
	wrapper.setCollection(coll)
}

func (reg *registry) solve(a float64, b float64, op rune) (float64, error) {
	if _, exists := reg.services[op]; !exists {
		return 0.0, fmt.Errorf("Service unavailable for operation %v", string(op))
	}

	if reg.services[op].length() == 0 {
		return 0.0, fmt.Errorf("Service unavailable for operation %v", string(op))
	}

	// TODO: Search for services until one of them isn't busy
	service, serviceErr := reg.services[op].getAvailableService()
	if serviceErr != nil {
		return 0.0, fmt.Errorf("Operation %v error: %v", string(op), serviceErr.Error())
	}

	pair := &ValuePair{
		A: a,
		B: b,
	}

	response, solveErr := service.Solve(context.Background(), pair)
	if solveErr != nil {
		return 0.0, solveErr
	}

	result := response.GetResult()
	return result, nil
}
