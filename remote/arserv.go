package remote

import (
	"context"
	"errors"
	"fmt"
)

type arService struct {
	addr string
	op   operation

	idle bool
}

func newArService(addr string, op operation) *arService {
	return &arService{
		addr: addr,
		op:   op,

		idle: true,
	}
}

func (service *arService) IsAvailable(context.Context, *AvailabilityRequest) (*AvailabilityResponse, error) {
	return &AvailabilityResponse{Status: service.idle}, nil
}

func (service *arService) Ping(context.Context, *PingRequest) (*PongResponse, error) {
	return &PongResponse{Status: true}, nil
}

func (service *arService) Solve(ctx context.Context, values *ValuePair) (*Response, error) {
	if !service.idle {
		return nil, errors.New("Trying to work while not idle")
	}
	service.idle = false
	done := make(chan struct{})
	var result float64
	var err error

	go func() {
		firstVal := values.GetA()
		secondVal := values.GetB()

		result, err = service.op.solve(firstVal, secondVal)
		fmt.Println("Sleeping for one minute")
		fmt.Println("Waking up")
		close(done)
	}()

	<-done
	service.idle = true

	if err != nil {
		return nil, err
	}

	return &Response{
		Result: result,
	}, nil
}
