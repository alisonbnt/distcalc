package remote

import (
	"context"
	"errors"
	"fmt"

	"google.golang.org/grpc"
)

type arServClientWrapper struct {
	addr                string
	service             ArithmeticServiceClient
	maxUnavailabilities int

	currUnavailabilities int
	coll                 *arServiceClientCollection
}

func newArServiceClientWrapper(addr string, service ArithmeticServiceClient, maxUnavailabilities int) *arServClientWrapper {
	return &arServClientWrapper{
		addr:                addr,
		service:             service,
		maxUnavailabilities: maxUnavailabilities,

		currUnavailabilities: 0,
	}
}

func (wrapper *arServClientWrapper) setCollection(coll *arServiceClientCollection) {
	wrapper.coll = coll
}

func (wrapper *arServClientWrapper) Solve(ctx context.Context, in *ValuePair, opts ...grpc.CallOption) (*Response, error) {
	// Solve errors can be relevant
	return wrapper.service.Solve(ctx, in, opts...)
}
func (wrapper *arServClientWrapper) Ping(ctx context.Context, in *PingRequest, opts ...grpc.CallOption) (*PongResponse, error) {
	pongRes, err := wrapper.service.Ping(ctx, in, opts...)
	if err == nil {
		return pongRes, err
	}
	wrapper.currUnavailabilities++
	if wrapper.currUnavailabilities > wrapper.maxUnavailabilities {
		fmt.Println("Too many errors, asking for removal")
		wrapper.coll.removeService(wrapper.addr)
		wrapper.coll = nil
	}

	return pongRes, err
}
func (wrapper *arServClientWrapper) IsAvailable(ctx context.Context, in *AvailabilityRequest, opts ...grpc.CallOption) (*AvailabilityResponse, error) {
	avalRes, err := wrapper.service.IsAvailable(ctx, in, opts...)

	if err == nil {
		return avalRes, err
	}

	wrapper.currUnavailabilities++
	fmt.Println("Available error", err.Error())
	if wrapper.currUnavailabilities > wrapper.maxUnavailabilities {
		fmt.Println("Too many errors, asking for removal")
		wrapper.coll.removeService(wrapper.addr)
		wrapper.coll = nil
	}

	return avalRes, err
}

type arServiceClientCollection struct {
	services map[string]ArithmeticServiceClient
}

func newArServiceClientCollection() *arServiceClientCollection {
	return &arServiceClientCollection{
		services: map[string]ArithmeticServiceClient{},
	}
}

func (collection *arServiceClientCollection) addService(addr string, service ArithmeticServiceClient) {
	collection.services[addr] = service
}

func (collection *arServiceClientCollection) removeService(addr string) {
	fmt.Println("Removing service", addr)
	delete(collection.services, addr)
}

func (collection *arServiceClientCollection) getAvailableService() (ArithmeticServiceClient, error) {
	var selectedService ArithmeticServiceClient
	for _, service := range collection.services {
		result, avalErr := service.IsAvailable(context.Background(), &AvailabilityRequest{})
		if avalErr == nil && result.GetStatus() {
			selectedService = service
			break
		}
	}

	if selectedService == nil {
		return nil, errors.New("No service available")
	}

	return selectedService, nil
}

func (collection *arServiceClientCollection) length() int {
	return len(collection.services)
}
