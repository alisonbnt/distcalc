package remote

import (
	"errors"
	"fmt"
)

type operation interface {
	getSymbol() rune
	solve(a float64, b float64) (float64, error)
}

type addOp struct{}

func (op *addOp) getSymbol() rune {
	return '+'
}

func (op *addOp) solve(a float64, b float64) (float64, error) {
	fmt.Println("Calling the + op")
	return a + b, nil
}

type subOp struct{}

func (op *subOp) getSymbol() rune {
	return '-'
}

func (op *subOp) solve(a float64, b float64) (float64, error) {
	fmt.Println("Calling the - op")
	return a - b, nil
}

type multOp struct{}

func (op *multOp) getSymbol() rune {
	return '*'
}

func (op *multOp) solve(a float64, b float64) (float64, error) {
	fmt.Println("Calling the * op")
	return a * b, nil
}

type divOp struct{}

func (op *divOp) getSymbol() rune {
	return '+'
}

func (op *divOp) solve(a float64, b float64) (float64, error) {
	fmt.Println("Calling the / op")
	if b == 0.0 {
		return 0.0, errors.New("Cannot divide by zero")
	}
	return a / b, nil
}
