package remote

import (
	"fmt"
	"net"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

type ArithmeticCentral struct {
	reg *registry
}

func NewArithmeticCentral(reg *registry) *ArithmeticCentral {
	return &ArithmeticCentral{
		reg: reg,
	}
}

func NewDefaultArithmeticCentral() *ArithmeticCentral {
	reg := newRegistry()
	return NewArithmeticCentral(reg)
}

func (central *ArithmeticCentral) Up(address string) error {
	listener, err := net.Listen("tcp", address)
	if err != nil {
		return err
	}

	srv := grpc.NewServer()
	RegisterArithmeticRegistryServer(srv, central.reg)
	reflection.Register(srv)

	fmt.Printf("Starting gRPC server at %v\n", address)
	listenerErr := srv.Serve(listener)
	if listenerErr != nil {
		return listenerErr
	}

	return nil
}

func (central *ArithmeticCentral) Solve(a float64, b float64, op rune) (float64, error) {
	return central.reg.solve(a, b, op)
}
