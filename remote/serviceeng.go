package remote

import (
	"context"
	"fmt"
	"net"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

type ServiceDaemon struct {
	masterAddr string
	op         string
	service    *arService

	client ArithmeticRegistryClient
}

func NewDefaultServiceDaemon(masterAddr string, serviceAddr string, opSign string) *ServiceDaemon {
	var op operation

	switch opSign {
	case "+":
		op = &addOp{}
	case "-":
		op = &subOp{}
	case "*":
		op = &multOp{}
	case "/":
		op = &divOp{}
	}

	return newServiceDaemon(masterAddr, opSign, newArService(serviceAddr, op))
}

func newServiceDaemon(masterAddr string, op string, service *arService) *ServiceDaemon {
	return &ServiceDaemon{
		masterAddr: masterAddr,
		op:         op,
		service:    service,
	}
}

func (daemon *ServiceDaemon) Up(serviceAddress string) error {
	ready := make(chan struct{})
	go daemon.startServiceServer(serviceAddress, ready)
	<-ready

	masterDialErr := daemon.createMasterClient()
	if masterDialErr != nil {
		return masterDialErr
	}

	desc := &ServiceDescription{
		Symbol:  daemon.op,
		Address: serviceAddress,
	}

	response, subscribeErr := daemon.client.Register(context.Background(), desc)

	if subscribeErr != nil {
		return subscribeErr
	}

	if !response.GetSuccessful() {
		return fmt.Errorf("Subscribe error: %v", response.GetMsg())
	}

	fmt.Println("Successfully subscribed")

	return nil
}

func (daemon *ServiceDaemon) createMasterClient() error {
	conn, err := grpc.Dial(daemon.masterAddr, grpc.WithInsecure())
	if err != nil {
		return err
	}

	daemon.client = NewArithmeticRegistryClient(conn)
	return nil
}

func (daemon *ServiceDaemon) startServiceServer(address string, ready chan struct{}) error {
	listener, err := net.Listen("tcp", address)
	if err != nil {
		return err
	}

	srv := grpc.NewServer()
	RegisterArithmeticServiceServer(srv, daemon.service)
	reflection.Register(srv)

	fmt.Printf("[DAEMON] Starting gRPC server at %v\n", address)
	// TODO: Capture the listener error and panic/recover when it happens
	go srv.Serve(listener)
	close(ready)
	return nil
}
