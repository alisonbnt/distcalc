package exprparse

import (
	"testing"

	"issiata.com/dev/distcalc/arithmetic"
)

func assertExpressionWithContext(t *testing.T, expression string, expectedResult float64, ctx map[rune]float64) {
	context := arithmetic.NewArithmeticContext()
	context.SetContext(arithmetic.ExprCtx(ctx))
	ar := arithmetic.NewArithmetic(
		&arithmetic.HardCodedEngine{},
		context,
	)

	notPub := &NotationPass{}
	notPub.SetNotation(convNotation)

	parser := NewMultipleNotationExprParser(notPub, ar)
	expr, err := parser.Parse(expression)
	assertErrorNil(t, err)

	exprVal, solveErr := expr.Solve()
	assertErrorNil(t, solveErr)

	if exprVal != expectedResult {
		t.Errorf("Assert error: expected %v, got %v", expectedResult, exprVal)
	}
}

func assertExpression(t *testing.T, expression string, expectedResult float64) {
	assertExpressionWithContext(t, expression, expectedResult, map[rune]float64{})
}

func TestConvertion(t *testing.T) {
	// TODO: Run there asserts in goroutines
	assertExpression(t, "5+2", 7.0)
	assertExpression(t, "2+3*4", 14.0)
	assertExpression(t, "2+3*4/2", 8.0)
	assertExpression(t, "(2+3)*(4/2)/(3+1)", 2.5)
	assertExpression(t, "(2+3*(5-3))*(4/2)/(3+1)", 4.0)
	assertExpression(t, "2+(5*(7-(2+3)))", 12.0)
}

func TestConvertionWithVariables(t *testing.T) {
	ctx := map[rune]float64{'x': 2, 'y': 3}
	// TODO: Run there asserts in goroutines
	assertExpressionWithContext(t, "x+y", 5.0, ctx)
	assertExpressionWithContext(t, "x+y*4", 14.0, ctx)
	assertExpressionWithContext(t, "x+y*4/2", 8.0, ctx)
	assertExpressionWithContext(t, "(x+y)*(4/2)/(3+1)", 2.5, ctx)
	assertExpressionWithContext(t, "(x+y*(5-3))*(4/2)/(3+1)", 4.0, ctx)
	assertExpressionWithContext(t, "x+(5*(7-(2+y)))", 12.0, ctx)
}
