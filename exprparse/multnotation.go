package exprparse

import (
	"fmt"

	"issiata.com/dev/distcalc/arithmetic"
)

const (
	convNotation      = "conventional"
	polishNotation    = "polish"
	revPolishNotation = "reverse-polish"
)

type NotationPass struct {
	notation string
}

func (notPass *NotationPass) SetNotation(notation string) {
	notPass.notation = notation
}

func (notPass *NotationPass) GetNotation() string {
	return notPass.notation
}

type MultipleNotationExprParser struct {
	converter *queueToExpressionConverter
	notPass   *NotationPass
}

func NewMultipleNotationExprParser(notPass *NotationPass, ar *arithmetic.Arithmetic) *MultipleNotationExprParser {
	return &MultipleNotationExprParser{
		converter: newQueueToExpressionConverter(ar),
		notPass:   notPass,
	}
}

func (parser *MultipleNotationExprParser) Parse(expr string) (arithmetic.Expression, error) {
	notation := parser.notPass.GetNotation()
	if notation == "" {
		notation = convNotation
	}

	var strExpr expression
	switch notation {
	case convNotation:
		strExpr = newInfixStrExpr(expr)
	case polishNotation:
		strExpr = newPolishStrExpr(expr)
	case revPolishNotation:
		strExpr = newRevPolishStrExpr(expr)
	default:
		return nil, fmt.Errorf("Unknown notation %v", notation)
	}

	tokenQueue, infixErr := strExpr.parse()
	if infixErr != nil {
		return nil, infixErr
	}

	return parser.converter.convert(tokenQueue)
}
