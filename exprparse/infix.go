package exprparse

import (
	"errors"
	"fmt"
	"unicode"
)

type InfixStrExpr struct {
	expr       string
	valueQueue *tokenQueue
	opStack    *tokenStack
}

func newInfixStrExpr(expr string) *InfixStrExpr {
	return &InfixStrExpr{
		expr:       expr,
		valueQueue: newTokenQueue(),
		opStack:    newTokenStack(),
	}
}

func (expression *InfixStrExpr) parse() (*tokenQueue, error) {
	runeSlice := []rune(expression.expr)
	strLen := len(runeSlice)
	var lastToken *token

	for index := 0; index < strLen; index++ {
		currRune := runeSlice[index]
		if currRune == ' ' {
			continue
		}
		neoLastToken, err := expression.processRune(index, currRune, lastToken)
		if err != nil {
			lastIndex := index
			if neoLastToken != nil {
				lastIndex = neoLastToken.pos
			}

			return nil, fmt.Errorf("Expression error at position %v: %v", lastIndex+1, err.Error())
		}

		lastToken = neoLastToken
	}

	if expression.valueQueue.isEmpty() {
		return nil, errors.New("Expression error: cannot calculate expression without values")
	}

	queueTail := expression.valueQueue.tail()
	if queueTail.getType() == runeBasedScalarToken {
		queueTail.compileRunes()
	}

	for !expression.opStack.isEmpty() {
		opToken, popErr := expression.opStack.pop()
		if popErr != nil {
			return nil, popErr
		}

		if opToken.getType() == openingParenthesis {
			return nil, fmt.Errorf("Expression error at position %v: Could not find matching close parenthesis on expression", opToken.pos+1)
		}

		expression.valueQueue.enqueue(opToken)
	}

	return expression.valueQueue, nil
}

func (expression *InfixStrExpr) processRune(index int, currRune rune, lastToken *token) (*token, error) {
	var currToken *token
	isValidToken := false

	if unicode.IsDigit(currRune) || currRune == '.' {
		isValidToken = true
		if expression.tailIsRuneBased() && !expression.valueQueue.tail().alreadyCompiledRunes() {
			currToken = expression.valueQueue.tail()
			currToken.appendRune(currRune)
		} else {
			currToken = newRuneBasedScalarToken(currRune, index)
			expression.valueQueue.enqueue(currToken)
		}
	} else if expression.tailIsRuneBased() {
		expression.valueQueue.tail().compileRunes()
	}

	if expression.runeIsOperator(currRune) {
		isValidToken = true
		currToken = newOperatorToken(currRune, index)
		opPushErr := expression.pushOperator(currToken)
		if opPushErr != nil {
			return nil, opPushErr
		}
	}

	if unicode.IsLetter(currRune) {
		isValidToken = true
		currToken = newVariableToken(currRune, index)
		if lastToken != nil && lastToken.isScalar() {
			timesOpToken := newOperatorToken('*', index)
			opPushErr := expression.pushOperator(timesOpToken)
			if opPushErr != nil {
				return nil, opPushErr
			}
		}
		expression.valueQueue.enqueue(currToken)
	}

	if currRune == '(' {
		isValidToken = true

		if lastToken != nil && (lastToken.getType() != operatorToken && lastToken.getType() != openingParenthesis) {
			return nil, fmt.Errorf("Opening parenthesis after '%v' token is not allowed", lastToken.toStrExpr())
		}

		currToken = newOpeningParenthesisToken(index)
		expression.opStack.push(currToken)
	}

	if currRune == ')' {
		isValidToken = true
		currToken = nil
		openingParFound := false
		for !openingParFound && !expression.opStack.isEmpty() {
			if expression.opStack.peek().getType() == openingParenthesis {
				openingParFound = true
				break
			}
			parenthesisPop, parenthesisErr := expression.opStack.pop()
			if parenthesisErr != nil {
				// TODO: Descrever melhor esse erro
				return nil, parenthesisErr
			}
			expression.valueQueue.enqueue(parenthesisPop)
		}

		if !openingParFound {
			return nil, errors.New("Could not find matching open parenthesis on expression")
		}

		if expression.opStack.peek().getType() == openingParenthesis {
			// We can safely ignore the opening parenthesis token here
			_, openPopErr := expression.opStack.pop()
			if openPopErr != nil {
				return nil, openPopErr
			}
		}
	}

	if !isValidToken {
		return nil, fmt.Errorf("Invalid token: %v", string(currRune))
	}

	return currToken, nil
}

func (expression *InfixStrExpr) pushOperator(opToken *token) error {
	topOp := expression.opStack.peek()

	for topOp != nil && topOp.getPrecedence() >= opToken.getPrecedence() {
		currPop, popErr := expression.opStack.pop()
		if popErr != nil {
			return popErr
		}
		expression.valueQueue.enqueue(currPop)
		topOp = expression.opStack.peek()
	}

	expression.opStack.push(opToken)
	return nil
}

func (expression *InfixStrExpr) tailIsRuneBased() bool {
	valQueueTail := expression.valueQueue.tail()
	return valQueueTail != nil && valQueueTail.getType() == runeBasedScalarToken
}

func (expression *InfixStrExpr) runeIsOperator(currRune rune) bool {
	return currRune == '+' || currRune == '-' || currRune == '*' || currRune == '/'
}
