package exprparse

import (
	"errors"
	"fmt"
	"unicode"
)

type polishExpr struct {
	expr       string
	valueStack *tokenStack
}

func newPolishStrExpr(expr string) *polishExpr {
	return &polishExpr{
		expr:       expr,
		valueStack: newTokenStack(),
	}
}

func (expression *polishExpr) parse() (*tokenQueue, error) {
	runeSlice := []rune(expression.expr)
	strLen := len(runeSlice)

	for index := strLen - 1; index >= 0; index-- {
		currRune := runeSlice[index]
		if currRune == ' ' {
			if expression.peekIsRuneBased() {
				expression.valueStack.peek().compileRunes()
			}
			continue
		}
		err := expression.processRune(index, currRune)
		if err != nil {
			lastIndex := index
			return nil, fmt.Errorf("Expression error at position %v: %v", lastIndex+1, err.Error())
		}
	}

	if expression.valueStack.isEmpty() {
		return nil, errors.New("Expression error: cannot calculate expression without values")
	}

	queue := newTokenQueue()

	topToken, topErr := expression.valueStack.pop()
	if topErr != nil {
		return nil, fmt.Errorf("Expression error: %v", topErr.Error())
	}

	if !expression.valueStack.isEmpty() {
		return nil, errors.New("Expression error: Stack not empty after parsing")
	}

	if topToken.getType() == compositeToken {
		topToken.populateRecursively(queue)
	} else {
		if topToken.getType() == runeBasedScalarToken && !topToken.alreadyCompiledRunes() {
			topToken.compileRunes()
		}

		queue.enqueue(topToken)
	}

	return queue, nil
}

func (expression *polishExpr) processRune(index int, currRune rune) error {
	isValidToken := false

	if unicode.IsDigit(currRune) || currRune == '.' {
		isValidToken = true
		if expression.peekIsRuneBased() && !expression.valueStack.peek().alreadyCompiledRunes() {
			expression.valueStack.peek().preppendRune(currRune)
		} else {
			runeToken := newRuneBasedScalarToken(currRune, index)
			expression.valueStack.push(runeToken)
		}
	} else if expression.peekIsRuneBased() {
		expression.valueStack.peek().compileRunes()
	}

	if expression.runeIsOperator(currRune) {
		isValidToken = true
		opToken := newOperatorToken(currRune, index)
		tokenA, popAErr := expression.valueStack.pop()
		if popAErr != nil {
			return popAErr
		}

		tokenB, popBErr := expression.valueStack.pop()
		if popBErr != nil {
			return popBErr
		}

		compToken := newCompositeToken(index, tokenA, tokenB, opToken)
		expression.valueStack.push(compToken)
	}

	if unicode.IsLetter(currRune) {
		isValidToken = true
		vToken := newVariableToken(currRune, index)
		expression.valueStack.push(vToken)
	}

	if !isValidToken {
		return fmt.Errorf("Invalid token: %v", string(currRune))
	}

	return nil
}

func (expression *polishExpr) peekIsRuneBased() bool {
	peekToken := expression.valueStack.peek()
	return peekToken != nil && peekToken.getType() == runeBasedScalarToken
}

func (expression *polishExpr) runeIsOperator(currRune rune) bool {
	return currRune == '+' || currRune == '-' || currRune == '*' || currRune == '/'
}
