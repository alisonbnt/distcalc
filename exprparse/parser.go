package exprparse

import (
	"errors"

	"issiata.com/dev/distcalc/arithmetic"
)

type StrExprParser interface {
	Parse(expr string) (arithmetic.Expression, error)
}

type queueToExpressionConverter struct {
	ar *arithmetic.Arithmetic
}

func newQueueToExpressionConverter(ar *arithmetic.Arithmetic) *queueToExpressionConverter {
	return &queueToExpressionConverter{
		ar: ar,
	}
}

func (converter *queueToExpressionConverter) convert(queue *tokenQueue) (arithmetic.Expression, error) {
	eStack := newExprStack()
	for !queue.isEmpty() {
		var expr arithmetic.Expression
		currToken, dequeueErr := queue.dequeue()

		if dequeueErr != nil {
			return nil, dequeueErr
		}

		if currToken.isScalar() && currToken.getType() != variableToken {
			expr = converter.ar.MakeScalarExpression(currToken.getValue())
		}

		if currToken.getType() == operatorToken {
			opRune := currToken.getOperator()
			a, aPopErr := eStack.pop()
			if aPopErr != nil {
				return nil, aPopErr
			}

			b, bPopErr := eStack.pop()
			if bPopErr != nil {
				return nil, bPopErr
			}
			expr = converter.ar.MakeOperationExpression(b, a, opRune)
		}

		if currToken.getType() == variableToken {
			expr = converter.ar.MakeVariableExpression(currToken.getVariableSymbol())
		}

		eStack.push(expr)
	}

	finalExpr, finalPopErr := eStack.pop()
	if finalPopErr != nil {
		return nil, finalPopErr
	}

	if !eStack.isEmpty() {
		return nil, errors.New("Expression could not be solved - Too many scalars to operators")
	}

	return finalExpr, nil
}
