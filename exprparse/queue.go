package exprparse

import (
	"errors"
)

type tokenQueue struct {
	tokens []*token
}

func newTokenQueue() *tokenQueue {
	return &tokenQueue{tokens: []*token{}}
}

func (queue *tokenQueue) enqueue(t *token) {
	queue.tokens = append(queue.tokens, t)
}

func (queue *tokenQueue) dequeue() (*token, error) {
	if queue.isEmpty() {
		return nil, errors.New("Cannot dequeue empty queue")
	}

	token := queue.tokens[0]
	queue.tokens[0] = nil
	queue.tokens = queue.tokens[1:]

	return token, nil
}

func (queue *tokenQueue) isEmpty() bool {
	return len(queue.tokens) == 0
}

func (queue *tokenQueue) head() *token {
	if queue.isEmpty() {
		return nil
	}

	return queue.tokens[0]
}

func (queue *tokenQueue) tail() *token {
	if queue.isEmpty() {
		return nil
	}

	return queue.tokens[len(queue.tokens)-1]
}

func (queue *tokenQueue) asStrExpr() string {
	expr := ""
	for _, t := range queue.tokens {
		expr = expr + t.toStrExpr()
	}

	return expr
}
