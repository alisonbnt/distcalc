package exprparse

import (
	"errors"

	"issiata.com/dev/distcalc/arithmetic"
)

type exprStack struct {
	expressions []arithmetic.Expression
	length      int
}

func newExprStack() *exprStack {
	return &exprStack{expressions: []arithmetic.Expression{}, length: 0}
}

func (stack *exprStack) push(expr arithmetic.Expression) {
	stack.expressions = append(stack.expressions, expr)
	stack.length = stack.length + 1
}

func (stack *exprStack) peek() arithmetic.Expression {
	if stack.isEmpty() {
		return nil
	}

	return stack.expressions[stack.length-1]
}

func (stack *exprStack) pop() (arithmetic.Expression, error) {
	if stack.isEmpty() {
		return nil, errors.New("Cannot pop an empty stack")
	}

	pop := stack.expressions[stack.length-1]
	stack.expressions[stack.length-1] = nil
	stack.expressions = stack.expressions[:stack.length-1]
	stack.length = stack.length - 1

	return pop, nil
}

func (stack *exprStack) isEmpty() bool {
	return stack.length == 0
}
