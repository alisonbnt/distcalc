package exprparse

import "testing"

func TestRevPolSimpleExpression(t *testing.T) {
	strExpr := "2"
	floatExprVal := 2.0
	expr := newRevPolishStrExpr(strExpr)
	tQueue, err := expr.parse()
	assertErrorNil(t, err)

	headVal := tQueue.head().getValue()
	if headVal != floatExprVal {
		t.Errorf("Wrong head value: expected %v, got %v", floatExprVal, headVal)
	}
}

func TestRevPolCompositeScalarExpression(t *testing.T) {
	strExpr := "234"
	floatExprVal := 234.0
	expr := newRevPolishStrExpr(strExpr)
	tQueue, err := expr.parse()
	assertErrorNil(t, err)

	headVal := tQueue.head().getValue()
	if headVal != floatExprVal {
		t.Errorf("Wrong head value: expected %v, got %v", floatExprVal, headVal)
	}
}

func TestRevPolSimpleOperation(t *testing.T) {
	strExpr := "2 3 +"
	expr := newRevPolishStrExpr(strExpr)
	tQueue, err := expr.parse()
	assertErrorNil(t, err)

	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 2.0})
	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 3.0})
	assertOnDequeue(t, tQueue, &operatorTokenAssert{op: '+'})
}

func TestRevPolBiggerNumbers(t *testing.T) {
	strExpr := "234 345 +"
	expr := newRevPolishStrExpr(strExpr)
	tQueue, err := expr.parse()
	assertErrorNil(t, err)

	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 234.0})
	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 345.0})
	assertOnDequeue(t, tQueue, &operatorTokenAssert{op: '+'})
}

func TestRevPolPrecedence(t *testing.T) {
	strExpr := "2 3 * 4 +"
	expr := newRevPolishStrExpr(strExpr)

	tQueue, err := expr.parse()
	assertErrorNil(t, err)

	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 2.0})
	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 3.0})
	assertOnDequeue(t, tQueue, &operatorTokenAssert{op: '*'})
	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 4.0})
	assertOnDequeue(t, tQueue, &operatorTokenAssert{op: '+'})
}

func TestRevPolPrecedenceAfter(t *testing.T) {
	strExpr := "2 3 4 * +"
	expr := newRevPolishStrExpr(strExpr)

	tQueue, err := expr.parse()
	assertErrorNil(t, err)

	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 2.0})
	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 3.0})
	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 4.0})
	assertOnDequeue(t, tQueue, &operatorTokenAssert{op: '*'})
	assertOnDequeue(t, tQueue, &operatorTokenAssert{op: '+'})
}

func TestRevPolComplexPrecedence(t *testing.T) {
	strExpr := "1 2 3 * - 4 5 / 6 * + 7 +"
	expr := newRevPolishStrExpr(strExpr)

	tQueue, err := expr.parse()
	assertErrorNil(t, err)

	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 1.0})
	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 2.0})
	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 3.0})
	assertOnDequeue(t, tQueue, &operatorTokenAssert{op: '*'})
	assertOnDequeue(t, tQueue, &operatorTokenAssert{op: '-'})
	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 4.0})
	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 5.0})
	assertOnDequeue(t, tQueue, &operatorTokenAssert{op: '/'})
	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 6.0})
	assertOnDequeue(t, tQueue, &operatorTokenAssert{op: '*'})
	assertOnDequeue(t, tQueue, &operatorTokenAssert{op: '+'})
	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 7.0})
	assertOnDequeue(t, tQueue, &operatorTokenAssert{op: '+'})
}

func TestRevPolVariable(t *testing.T) {
	strExpr := "2 x +"
	expr := newRevPolishStrExpr(strExpr)

	tQueue, err := expr.parse()
	assertErrorNil(t, err)

	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 2.0})
	assertOnDequeue(t, tQueue, &variableTokenAssert{symbol: 'x'})
	assertOnDequeue(t, tQueue, &operatorTokenAssert{op: '+'})
}
