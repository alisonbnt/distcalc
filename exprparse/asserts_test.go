package exprparse

import (
	"testing"
)

func assertPeekValue(t *testing.T, stack *tokenStack, expected float64) {
	peekVal := stack.peek().getValue()
	if peekVal != expected {
		t.Errorf("Wrong peek value: expected %v, got %v", expected, peekVal)
	}
}

func assertErrorNil(t *testing.T, err error) {
	if err != nil {
		t.Errorf("Unexpected error: %v", err.Error())
	}
}

type tokenAssert interface {
	execute(t *testing.T, token *token)
}

type scalarTokenAssert struct {
	val float64
}

func (assert *scalarTokenAssert) execute(t *testing.T, token *token) {
	if !token.isScalar() && token.getValue() != assert.val {
		t.Errorf("Expected [scalar<%v>], got %v", assert.val, token.toString())
	}
}

type operatorTokenAssert struct {
	op rune
}

func (assert *operatorTokenAssert) execute(t *testing.T, token *token) {
	if token.getType() != operatorToken || token.getOperator() != assert.op {
		t.Errorf("Expected [operator<%v>], got %v", string(assert.op), token.toString())
	}
}

func assertOnDequeue(t *testing.T, queue *tokenQueue, assert tokenAssert) {
	el, elErr := queue.dequeue()
	if el == nil {
		t.Error("WTF, el nil")
	}
	assertErrorNil(t, elErr)
	assert.execute(t, el)
}

type variableTokenAssert struct {
	symbol rune
}

func (assert *variableTokenAssert) execute(t *testing.T, token *token) {
	if token.getType() != variableToken || token.getVariableSymbol() != assert.symbol {
		t.Errorf("Expected [variable<%v>], got %v", string(assert.symbol), token.toString())
	}
}
