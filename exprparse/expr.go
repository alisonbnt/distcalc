package exprparse

type expression interface {
	parse() (*tokenQueue, error)
}
