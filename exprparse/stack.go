package exprparse

import (
	"errors"
)

type tokenStack struct {
	tokens []*token
	length int
}

func newTokenStack() *tokenStack {
	return &tokenStack{tokens: []*token{}, length: 0}
}

func (stack *tokenStack) push(t *token) {
	stack.tokens = append(stack.tokens, t)
	stack.length = stack.length + 1
}

func (stack *tokenStack) peek() *token {
	if stack.isEmpty() {
		return nil
	}

	return stack.tokens[stack.length-1]
}

func (stack *tokenStack) pop() (*token, error) {
	if stack.isEmpty() {
		return nil, errors.New("Cannot pop an empty stack")
	}

	pop := stack.tokens[stack.length-1]
	stack.tokens[stack.length-1] = nil
	stack.tokens = stack.tokens[:stack.length-1]
	stack.length = stack.length - 1

	return pop, nil
}

func (stack *tokenStack) isEmpty() bool {
	return stack.length == 0
}
