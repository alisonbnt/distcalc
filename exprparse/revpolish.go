package exprparse

import (
	"errors"
	"fmt"
	"unicode"
)

type revPolishExpr struct {
	expr       string
	valueQueue *tokenQueue
}

func newRevPolishStrExpr(expr string) *revPolishExpr {
	return &revPolishExpr{
		expr:       expr,
		valueQueue: newTokenQueue(),
	}
}

func (expression *revPolishExpr) parse() (*tokenQueue, error) {
	runeSlice := []rune(expression.expr)
	strLen := len(runeSlice)

	for index := 0; index < strLen; index++ {
		currRune := runeSlice[index]
		if currRune == ' ' {
			if expression.tailIsRuneBased() {
				expression.valueQueue.tail().compileRunes()
			}
			continue
		}
		err := expression.processRune(index, currRune)
		if err != nil {
			lastIndex := index
			return nil, fmt.Errorf("Expression error at position %v: %v", lastIndex+1, err.Error())
		}
	}

	if expression.valueQueue.isEmpty() {
		return nil, errors.New("Expression error: cannot calculate expression without values")
	}

	queueTail := expression.valueQueue.tail()
	if queueTail.getType() == runeBasedScalarToken {
		queueTail.compileRunes()
	}

	return expression.valueQueue, nil
}

func (expression *revPolishExpr) processRune(index int, currRune rune) error {
	isValidToken := false

	if unicode.IsDigit(currRune) || currRune == '.' {
		isValidToken = true
		if expression.tailIsRuneBased() && !expression.valueQueue.tail().alreadyCompiledRunes() {
			expression.valueQueue.tail().appendRune(currRune)
		} else {
			runeToken := newRuneBasedScalarToken(currRune, index)
			expression.valueQueue.enqueue(runeToken)
		}
	} else if expression.tailIsRuneBased() {
		expression.valueQueue.tail().compileRunes()
	}

	if expression.runeIsOperator(currRune) {
		isValidToken = true
		opToken := newOperatorToken(currRune, index)
		expression.valueQueue.enqueue(opToken)
	}

	if unicode.IsLetter(currRune) {
		isValidToken = true
		vToken := newVariableToken(currRune, index)
		expression.valueQueue.enqueue(vToken)
	}

	if !isValidToken {
		return fmt.Errorf("Invalid token: %v", string(currRune))
	}

	return nil
}

func (expression *revPolishExpr) tailIsRuneBased() bool {
	valQueueTail := expression.valueQueue.tail()
	return valQueueTail != nil && valQueueTail.getType() == runeBasedScalarToken
}

func (expression *revPolishExpr) runeIsOperator(currRune rune) bool {
	return currRune == '+' || currRune == '-' || currRune == '*' || currRune == '/'
}
