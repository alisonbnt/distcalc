package exprparse

import "testing"

func TestPolSimpleExpression(t *testing.T) {
	strExpr := "2"
	floatExprVal := 2.0
	expr := newPolishStrExpr(strExpr)
	tQueue, err := expr.parse()
	assertErrorNil(t, err)

	headVal := tQueue.head().getValue()
	if headVal != floatExprVal {
		t.Errorf("Wrong head value: expected %v, got %v", floatExprVal, headVal)
	}
}

func TestPolCompositeScalarExpression(t *testing.T) {
	strExpr := "234"
	floatExprVal := 234.0
	expr := newPolishStrExpr(strExpr)
	tQueue, err := expr.parse()
	assertErrorNil(t, err)

	headVal := tQueue.head().getValue()
	if headVal != floatExprVal {
		t.Errorf("Wrong head value: expected %v, got %v", floatExprVal, headVal)
	}
}

func TestPolSimpleOperation(t *testing.T) {
	strExpr := "+ 2 3"
	expr := newPolishStrExpr(strExpr)
	tQueue, err := expr.parse()
	assertErrorNil(t, err)

	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 2.0})
	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 3.0})
	assertOnDequeue(t, tQueue, &operatorTokenAssert{op: '+'})
}

func TestPolBiggerNumbers(t *testing.T) {
	strExpr := "+ 234 345"
	expr := newPolishStrExpr(strExpr)
	tQueue, err := expr.parse()
	assertErrorNil(t, err)

	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 234.0})
	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 345.0})
	assertOnDequeue(t, tQueue, &operatorTokenAssert{op: '+'})
}

func TestPolPrecedence(t *testing.T) {
	strExpr := "+ * 2 3 4"
	expr := newPolishStrExpr(strExpr)

	tQueue, err := expr.parse()
	assertErrorNil(t, err)

	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 2.0})
	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 3.0})
	assertOnDequeue(t, tQueue, &operatorTokenAssert{op: '*'})
	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 4.0})
	assertOnDequeue(t, tQueue, &operatorTokenAssert{op: '+'})
}

func TestPolPrecedenceAfter(t *testing.T) {
	strExpr := "+ 2 * 3 4"
	expr := newPolishStrExpr(strExpr)

	tQueue, err := expr.parse()
	assertErrorNil(t, err)

	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 2.0})
	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 3.0})
	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 4.0})
	assertOnDequeue(t, tQueue, &operatorTokenAssert{op: '*'})
	assertOnDequeue(t, tQueue, &operatorTokenAssert{op: '+'})
}

func TestPolVariable(t *testing.T) {
	strExpr := "+ 2 x"
	expr := newPolishStrExpr(strExpr)

	tQueue, err := expr.parse()
	assertErrorNil(t, err)

	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 2.0})
	assertOnDequeue(t, tQueue, &variableTokenAssert{symbol: 'x'})
	assertOnDequeue(t, tQueue, &operatorTokenAssert{op: '+'})
}
