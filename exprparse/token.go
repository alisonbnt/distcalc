package exprparse

import (
	"fmt"
	"strconv"
)

const (
	openingParenthesis   = "openParenthesis"
	scalarToken          = "scalar"
	operatorToken        = "operator"
	variableToken        = "variable"
	runeBasedScalarToken = "runescalar"
	compositeToken       = "composite"
)

type token struct {
	tokenType string
	pos       int

	// RuneScalar
	isStringComplete bool
	strVal           string

	// Operator
	operator rune

	// Scalar
	value float64

	// Variable
	variableSymbol rune

	// Composite
	children []*token
}

func newRuneBasedScalarToken(firstChar rune, pos int) *token {
	return &token{
		tokenType:        runeBasedScalarToken,
		pos:              pos,
		isStringComplete: false,
		strVal:           string(firstChar),
	}
}

func newScalarToken(value float64, pos int) *token {
	return &token{
		tokenType:        scalarToken,
		pos:              pos,
		isStringComplete: true,
		value:            value,
	}
}

func newOperatorToken(op rune, pos int) *token {
	return &token{
		tokenType: operatorToken,
		pos:       pos,
		operator:  op,
	}
}

func newVariableToken(symbol rune, pos int) *token {
	return &token{
		tokenType:      variableToken,
		pos:            pos,
		variableSymbol: symbol,
	}
}

func newOpeningParenthesisToken(pos int) *token {
	return &token{
		tokenType: openingParenthesis,
		pos:       pos,
	}
}

func newCompositeToken(pos int, tokens ...*token) *token {
	return &token{
		tokenType: compositeToken,
		pos:       pos,
		children:  tokens,
	}
}

func (t *token) getType() string {
	return t.tokenType
}

func (t *token) isScalar() bool {
	return t.tokenType == scalarToken || t.tokenType == runeBasedScalarToken || t.tokenType == variableToken
}

func (t *token) appendRune(val rune) {
	t.strVal = t.strVal + string(val)
}

func (t *token) preppendRune(val rune) {
	t.strVal = string(val) + t.strVal
}

func (t *token) alreadyCompiledRunes() bool {
	return t.isStringComplete
}

func (t *token) compileRunes() error {
	t.isStringComplete = true

	numVal, err := strconv.ParseFloat(t.strVal, 64)
	if err != nil {
		return fmt.Errorf("Cannot convert %v to float: %v", t.strVal, err.Error())
	}

	t.value = numVal
	return nil
}

func (t *token) getValue() float64 {
	return t.value
}

func (t *token) getOperator() rune {
	return t.operator
}

func (t *token) getPrecedence() int {
	if t.operator == '+' || t.operator == '-' {
		return 1
	}

	if t.operator == '*' || t.operator == '/' {
		return 2
	}

	return 0
}

func (t *token) getVariableSymbol() rune {
	return t.variableSymbol
}

func (t *token) populateRecursively(queue *tokenQueue) {
	for _, child := range t.children {
		if child.getType() == compositeToken {
			child.populateRecursively(queue)
		} else {
			queue.enqueue(child)
		}
	}
}

func (t *token) toString() string {
	return fmt.Sprintf("[%v<%v>]", t.getType(), t.toStrExpr())
}

func (t *token) toStrExpr() string {
	content := ""
	switch t.getType() {
	case scalarToken:
	case runeBasedScalarToken:
		content = fmt.Sprintf("%v", t.getValue())
	case operatorToken:
		content = string(t.getOperator())
	case variableToken:
		content = string(t.getVariableSymbol())
	}

	return fmt.Sprintf("%v", content)
}
