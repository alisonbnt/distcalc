package exprparse

import "testing"

func TestStack(t *testing.T) {
	stack := newTokenStack()

	if !stack.isEmpty() {
		t.Error("Stack not empty uppon creation")
	}

	tokenVal := 3.0
	stack.push(newScalarToken(tokenVal, 0))
	assertPeekValue(t, stack, tokenVal)

	neoTokenVal := 4.0
	stack.push(newScalarToken(neoTokenVal, 0))
	assertPeekValue(t, stack, neoTokenVal)

	popOne, popOneErr := stack.pop()
	assertErrorNil(t, popOneErr)
	popOneVal := popOne.getValue()

	if popOneVal != neoTokenVal {
		t.Errorf("Wrong pop value: expected %v, got %v", neoTokenVal, popOneVal)
	}

	popTwo, popTwoErr := stack.pop()
	assertErrorNil(t, popTwoErr)
	popTwoVal := popTwo.getValue()

	if popTwoVal != tokenVal {
		t.Errorf("Wrong pop value: expected %v, got %v", tokenVal, popTwoVal)
	}

	if !stack.isEmpty() {
		t.Error("Stack not empty after full pop")
	}
}

func TestEmptyStackError(t *testing.T) {
	stack := newTokenStack()

	_, err := stack.pop()
	if err == nil {
		t.Error("Expected error, got nil")
	}
}
