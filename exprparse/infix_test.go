package exprparse

import (
	"testing"
)

func TestSimpleExpression(t *testing.T) {
	strExpr := "2"
	floatExprVal := 2.0
	expr := newInfixStrExpr(strExpr)
	tQueue, err := expr.parse()
	assertErrorNil(t, err)

	headVal := tQueue.head().getValue()
	if headVal != floatExprVal {
		t.Errorf("Wrong head value: expected %v, got %v", floatExprVal, headVal)
	}
}

func TestCompositeScalarExpression(t *testing.T) {
	strExpr := "234"
	floatExprVal := 234.0
	expr := newInfixStrExpr(strExpr)
	tQueue, err := expr.parse()
	assertErrorNil(t, err)

	headVal := tQueue.head().getValue()
	if headVal != floatExprVal {
		t.Errorf("Wrong head value: expected %v, got %v", floatExprVal, headVal)
	}
}

func TestSimpleOperation(t *testing.T) {
	strExpr := "2+3"
	expr := newInfixStrExpr(strExpr)
	tQueue, err := expr.parse()
	assertErrorNil(t, err)

	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 2.0})
	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 3.0})
	assertOnDequeue(t, tQueue, &operatorTokenAssert{op: '+'})
}

func TestBiggerNumbers(t *testing.T) {
	strExpr := "234+345"
	expr := newInfixStrExpr(strExpr)
	tQueue, err := expr.parse()
	assertErrorNil(t, err)

	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 234.0})
	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 345.0})
	assertOnDequeue(t, tQueue, &operatorTokenAssert{op: '+'})
}

func TestPrecedence(t *testing.T) {
	strExpr := "2*3+4"
	expr := newInfixStrExpr(strExpr)

	tQueue, err := expr.parse()
	assertErrorNil(t, err)

	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 2.0})
	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 3.0})
	assertOnDequeue(t, tQueue, &operatorTokenAssert{op: '*'})
	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 4.0})
	assertOnDequeue(t, tQueue, &operatorTokenAssert{op: '+'})
}

func TestPrecedenceAfter(t *testing.T) {
	strExpr := "2+3*4"
	expr := newInfixStrExpr(strExpr)

	tQueue, err := expr.parse()
	assertErrorNil(t, err)

	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 2.0})
	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 3.0})
	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 4.0})
	assertOnDequeue(t, tQueue, &operatorTokenAssert{op: '*'})
	assertOnDequeue(t, tQueue, &operatorTokenAssert{op: '+'})
}

func TestComplexPrecedence(t *testing.T) {
	strExpr := "1-2*3+4/5*6+7"
	expr := newInfixStrExpr(strExpr)

	tQueue, err := expr.parse()
	assertErrorNil(t, err)

	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 1.0})
	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 2.0})
	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 3.0})
	assertOnDequeue(t, tQueue, &operatorTokenAssert{op: '*'})
	assertOnDequeue(t, tQueue, &operatorTokenAssert{op: '-'})
	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 4.0})
	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 5.0})
	assertOnDequeue(t, tQueue, &operatorTokenAssert{op: '/'})
	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 6.0})
	assertOnDequeue(t, tQueue, &operatorTokenAssert{op: '*'})
	assertOnDequeue(t, tQueue, &operatorTokenAssert{op: '+'})
	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 7.0})
	assertOnDequeue(t, tQueue, &operatorTokenAssert{op: '+'})
}

func TestParenthesis(t *testing.T) {
	strExpr := "5*(3+2)"
	expr := newInfixStrExpr(strExpr)

	tQueue, err := expr.parse()
	assertErrorNil(t, err)

	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 5.0})
	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 3.0})
	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 2.0})
	assertOnDequeue(t, tQueue, &operatorTokenAssert{op: '+'})
	assertOnDequeue(t, tQueue, &operatorTokenAssert{op: '*'})
}

func TestMoreComplexParenthesis(t *testing.T) {
	strExpr := "5+(3+2*5-1)*(5+2)"
	expr := newInfixStrExpr(strExpr)

	tQueue, err := expr.parse()
	assertErrorNil(t, err)
	// 5+(3+2*5-1)*(5+2)
	// 5 3 2 5 * + 1 - 5 2 + * +

	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 5.0})
	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 3.0})
	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 2.0})
	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 5.0})
	assertOnDequeue(t, tQueue, &operatorTokenAssert{op: '*'})
	assertOnDequeue(t, tQueue, &operatorTokenAssert{op: '+'})
	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 1.0})
	assertOnDequeue(t, tQueue, &operatorTokenAssert{op: '-'})
	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 5.0})
	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 2.0})
	assertOnDequeue(t, tQueue, &operatorTokenAssert{op: '+'})
	assertOnDequeue(t, tQueue, &operatorTokenAssert{op: '*'})
	assertOnDequeue(t, tQueue, &operatorTokenAssert{op: '+'})
}

func TestVariable(t *testing.T) {
	strExpr := "2+x"
	expr := newInfixStrExpr(strExpr)

	tQueue, err := expr.parse()
	assertErrorNil(t, err)

	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 2.0})
	assertOnDequeue(t, tQueue, &variableTokenAssert{symbol: 'x'})
	assertOnDequeue(t, tQueue, &operatorTokenAssert{op: '+'})
}

func TestVariableTimesScalar(t *testing.T) {
	strExpr := "2x"
	expr := newInfixStrExpr(strExpr)

	tQueue, err := expr.parse()
	assertErrorNil(t, err)

	assertOnDequeue(t, tQueue, &scalarTokenAssert{val: 2.0})
	assertOnDequeue(t, tQueue, &variableTokenAssert{symbol: 'x'})
	assertOnDequeue(t, tQueue, &operatorTokenAssert{op: '*'})
}

func TestVariableTimesVariable(t *testing.T) {
	strExpr := "yx"
	expr := newInfixStrExpr(strExpr)

	tQueue, err := expr.parse()
	assertErrorNil(t, err)

	assertOnDequeue(t, tQueue, &variableTokenAssert{symbol: 'y'})
	assertOnDequeue(t, tQueue, &variableTokenAssert{symbol: 'x'})
	assertOnDequeue(t, tQueue, &operatorTokenAssert{op: '*'})
}
