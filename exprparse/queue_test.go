package exprparse

import (
	"testing"
)

func TestQueue(t *testing.T) {
	queue := newTokenQueue()

	if !queue.isEmpty() {
		t.Error("Queue not empty on create")
	}

	tokenVal := 3.0
	queue.enqueue(newScalarToken(tokenVal, 0))

	if queue.isEmpty() {
		t.Error("Queue empty after enqueue")
	}

	peekVal := queue.head().getValue()
	if peekVal != tokenVal {
		t.Error("Wrong enqueued value")
	}

	dequeued, err := queue.dequeue()

	if err != nil {
		t.Errorf("Unexpected error while dequeueing: %v", err.Error())
	}

	if !queue.isEmpty() {
		t.Error("Expected empty queue after dequeueing")
	}

	if dequeued.getValue() != tokenVal {
		t.Error("Wrong element dequeued")
	}
}

func TestEmptyQueueError(t *testing.T) {
	queue := newTokenQueue()

	_, err := queue.dequeue()
	if err == nil {
		t.Error("Expected error while dequeueing, no error throwed")
	}
}
