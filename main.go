package main

import (
	"flag"

	"issiata.com/dev/distcalc/app"
)

func main() {
	var isLocal bool
	flag.BoolVar(&isLocal, "local", false, "Disable distributed services")

	var httpAddr string
	flag.StringVar(&httpAddr, "httpAddr", "127.0.0.1:8000", "The http server bind address")

	var gRPCAddr string
	flag.StringVar(&gRPCAddr, "gRPCAddr", "127.0.0.1:1307", "The gRPC registry address")

	var service string
	flag.StringVar(&service, "service", "", "Starts the app as an arithmetic service")

	var serviceAddr string
	flag.StringVar(&serviceAddr, "serviceAddr", "127.0.0.1:4719", "The service bind address")

	flag.Parse()

	myApp := app.NewApp(httpAddr, gRPCAddr)
	if service == "" {
		startAppAsMaster(myApp, isLocal)
	} else {
		startAppAsService(myApp, service, serviceAddr)
	}

}

func startAppAsMaster(myApp *app.App, asLocal bool) {
	myApp.Up(asLocal)
}

func startAppAsService(myApp *app.App, service string, serviceAddr string) {
	myApp.StartService(service, serviceAddr)
}
