package arithmetic

import (
	"fmt"
)

type Engine interface {
	runOperation(a Expression, b Expression, op rune) (float64, error)
}

// THIS IS A HARDCODED ENGINE - Used for testing only
type HardCodedEngine struct{}

func (engine *HardCodedEngine) runOperation(a Expression, b Expression, op rune) (float64, error) {

	aVal, aErr := a.Solve()
	if aErr != nil {
		return 0.0, fmt.Errorf("Operation error: %v", aErr.Error())
	}

	bVal, bErr := b.Solve()
	if bErr != nil {
		return 0.0, fmt.Errorf("Operation error: %v", bErr.Error())
	}

	if op == '+' {
		return aVal + bVal, nil
	}

	if op == '-' {
		return aVal - bVal, nil
	}

	if op == '*' {
		return aVal * bVal, nil
	}

	if op == '/' {
		// Division by zero must be catched automatically
		return aVal / bVal, nil
	}

	return 0.0, fmt.Errorf("Unknown operator %v", op)
}
