package arithmetic

type ArithmeticContext struct {
	context ExprCtx
}

func NewArithmeticContext() *ArithmeticContext {
	return &ArithmeticContext{
		context: ExprCtx(map[rune]float64{}),
	}
}

type Arithmetic struct {
	eng     Engine
	context *ArithmeticContext
}

func (ctx *ArithmeticContext) SetContext(context ExprCtx) {
	ctx.context = context
}

func (ctx *ArithmeticContext) GetContext() ExprCtx {
	return ctx.context
}

func NewArithmetic(eng Engine, ctx *ArithmeticContext) *Arithmetic {
	return &Arithmetic{
		eng:     eng,
		context: ctx,
	}
}

func (ar *Arithmetic) MakeScalarExpression(value float64) Expression {
	return NewScalarExpr(value)
}

func (ar *Arithmetic) MakeOperationExpression(a Expression, b Expression, op rune) Expression {
	return NewOperationExpr(a, b, op, ar.eng)
}

func (ar *Arithmetic) MakeVariableExpression(symbol rune) Expression {
	return NewVariableExpr(symbol, ar.context.GetContext())
}
