package arithmetic

// THE SCALAR EXPRESSION
// The simpliest expression ever. No variables, no operators, just a value
type ScalarExpr struct {
	value float64
}

func NewScalarExpr(value float64) *ScalarExpr {
	return &ScalarExpr{
		value: value,
	}
}

func (scalar *ScalarExpr) Solve() (float64, error) {
	return scalar.value, nil
}
