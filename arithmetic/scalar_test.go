package arithmetic

import (
	"testing"
)

func TestScalarExpression(t *testing.T) {
	scalarVal := 5.0
	expression := NewScalarExpr(scalarVal)
	assertExprResult(t, expression, scalarVal)
}
