package arithmetic

import "fmt"

// THE VARIABLE EXPRESSION
// Defined by a char, this value changes based on context
type undefinedSymbolAtCtxError struct {
	symbol rune
}

func (err *undefinedSymbolAtCtxError) Error() string {
	return fmt.Sprintf("Undefined variable found: %v", string(err.symbol))
}

type ExprCtx map[rune]float64

func (eCtx ExprCtx) findValueFor(symbol rune) (float64, error) {
	if val, ok := eCtx[symbol]; ok {
		return val, nil
	}

	return 0.0, &undefinedSymbolAtCtxError{symbol: symbol}
}

type VariableExpr struct {
	symbol rune
	eCtx   ExprCtx
}

func NewVariableExpr(symbol rune, eCtx ExprCtx) *VariableExpr {
	return &VariableExpr{
		symbol: symbol,
		eCtx:   eCtx,
	}
}

func (varExpr *VariableExpr) Solve() (float64, error) {
	varVal, err := varExpr.eCtx.findValueFor(varExpr.symbol)

	if err != nil {
		return 0.0, err
	}

	return varVal, nil
}
