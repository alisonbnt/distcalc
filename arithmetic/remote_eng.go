package arithmetic

import (
	"fmt"

	"issiata.com/dev/distcalc/remote"
)

type RemoteEngine struct {
	central *remote.ArithmeticCentral
}

func NewRemoteEngine(central *remote.ArithmeticCentral) *RemoteEngine {
	return &RemoteEngine{
		central: central,
	}
}

func (eng *RemoteEngine) runOperation(a Expression, b Expression, op rune) (float64, error) {
	aVal, aErr := a.Solve()
	if aErr != nil {
		return 0.0, fmt.Errorf("Operation error: %v", aErr.Error())
	}

	bVal, bErr := b.Solve()
	if bErr != nil {
		return 0.0, fmt.Errorf("Operation error: %v", bErr.Error())
	}

	return eng.central.Solve(aVal, bVal, op)
}
