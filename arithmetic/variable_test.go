package arithmetic

import (
	"testing"
)

func TestVariableExpression(t *testing.T) {
	variable := 'a'
	variableVal := 5.0
	context := ExprCtx{
		variable: variableVal,
	}

	varExpr := NewVariableExpr(variable, context)
	assertExprResult(t, varExpr, variableVal)
}

func TestUndefinedVariableError(t *testing.T) {
	variable := 'c'
	context := ExprCtx{}
	varExpr := NewVariableExpr(variable, context)

	_, err := varExpr.Solve()

	if err == nil {
		t.Error("Expected error, got nil")
	}

	if _, ok := err.(*undefinedSymbolAtCtxError); !ok {
		t.Error("Expected 'undefinedSymbolAtCtxError', got another error")
	}
}
