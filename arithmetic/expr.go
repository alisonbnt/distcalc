package arithmetic

// THE EXPRESSION INTERFACE
type Expression interface {
	Solve() (float64, error) // can return an error since some operations might be impossible to perform
}

type OperationExpr struct {
	a   Expression
	b   Expression
	op  rune
	eng Engine
}

func NewOperationExpr(a Expression, b Expression, op rune, eng Engine) *OperationExpr {
	return &OperationExpr{
		a:   a,
		b:   b,
		op:  op,
		eng: eng,
	}
}

func (opExpr *OperationExpr) Solve() (float64, error) {
	return opExpr.eng.runOperation(opExpr.a, opExpr.b, opExpr.op)
}
