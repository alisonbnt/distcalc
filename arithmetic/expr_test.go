package arithmetic

import (
	"testing"
)

func assertExprResult(t *testing.T, expr Expression, expectedVal float64) {
	exprResult, err := expr.Solve()

	if err != nil {
		t.Errorf("Solve error: %v", err.Error())
	}

	if exprResult != exprResult {
		t.Errorf("Assert error: Expr Solve() expected %v, got %v", expectedVal, exprResult)
	}
}

func TestPlusOperation(t *testing.T) {
	expected := 2.0 + 3.0
	eng := &HardCodedEngine{}
	expr := NewOperationExpr(NewScalarExpr(2.0), NewScalarExpr(3.0), '+', eng)

	assertExprResult(t, expr, expected)
}

func TestInvalidOperator(t *testing.T) {
	eng := &HardCodedEngine{}
	expr := NewOperationExpr(NewScalarExpr(2.0), NewScalarExpr(3.0), '#', eng)

	_, err := expr.Solve()

	if err == nil {
		t.Error("Expected error, got nil")
	}
}

func TestNestedOperations(t *testing.T) {
	eng := &HardCodedEngine{}
	expected := 5.0 * (2.0 + 3.0)
	scalar := NewScalarExpr(5.0)
	expr := NewOperationExpr(
		NewScalarExpr(2.0),
		NewScalarExpr(3.0),
		'+',
		eng,
	)
	nestedExpr := NewOperationExpr(
		scalar,
		expr,
		'*',
		eng,
	)

	assertExprResult(t, nestedExpr, expected)
}

func TestNestedOperationsWithVariables(t *testing.T) {
	eng := &HardCodedEngine{}
	ctx := ExprCtx{'x': 3.0}

	expected := (2.0 * 3.0) + (10.0 / 2.0)

	firstExpr := NewOperationExpr(
		NewScalarExpr(2.0),
		NewVariableExpr('x', ctx),
		'*',
		eng,
	)

	secondExpr := NewOperationExpr(
		NewScalarExpr(10.0),
		NewScalarExpr(2.0),
		'/',
		eng,
	)

	finalExpr := NewOperationExpr(
		firstExpr,
		secondExpr,
		'+',
		eng,
	)

	assertExprResult(t, finalExpr, expected)
}
