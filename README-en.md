DISTCALC
========

If there is something that is a true proof of intelligence, it is the realization of
calculations. Adding, subtracting, multiplying and dividing are basic operations, but
managed to change the course of humanity. Now, imagine being able to perform
mathematical operations in a distributed way? This project is for this!

## Usage
There is a docker-compose which uses the distributed infrastructure for expression solving.
```sh
$ docker-compose up
```

Also, for testing purposes, one can build the project and use the local engine (single node without gRPC communication)
```sh
$ go build
$ ./distcalc -local
```

By default, the app exposes `127.0.0.1:8000`. This can be overwriten with the `-httpAddr` flag
```sh
$ ./distcalc -local -httpAddr 0.0.0.0:8080
```

In a distributed fashion, this project need a main node and service nodes. Here's a simple command to spawn a main node and
a adding service:
```sh
./distcalc & ./distcalc -service=+
```

## Solution methodology
We have two domains to model for this project:
- Resolution of mathematical expressions
- Parsing of mathematical expressions (depends on the previous one)

Also, there are support domains to address:
- gRPC-based communication
- HTTP server with a single endpoint (and reading the request parameters)
- Service Discovery and Load Balacing of services

The selected approach is to resolve the main domains using
Object Orientation and then use the support domains as services.
That way we are able to guarantee the correctness of the service, 
its accessibility and scalability.

## First step - Tackling expressions
1. Generate test cases that represent expressions in the form of objects and
   check that they resolve correctly
2. Implement test objects
3. Expand tests with more complex expressions
4. Carry out implementations related to the new tests


## Second step - String parsing
1. This package must have a main interface defined as `(string) => solvable`
2. The solvable type and the expression type are equivalent (using go inference)
3. Implement three parsers, one for each type of notation.
4. Create tests with simple expressions and move on to more complex expressions

## Third step - HTTP endpoint
1. We decided to use mux for routing, as we have plans to expand
   the http service in the future

At that point some design decisions were made
- We chose to parse string expressions for tokens using the
    Shunting-yard algorithm
- The initial idea was to convert the string directly to `arithmetic.Expression`,
    however, this generated a strong dependency between the packages. We chose to create
    a medium step, at which we would convert the expression string in a row of 
    tokens. Finally, we would convert the row of tokens to `arithmetic.Expression`. This decision 
    increases the overall complexity of the algorithm, but increases stability -
    we can test the `exprparse` package individually
- We opted to complete the error cases and alternative paths in the future,
    given that it is still necessary to create the distributed structure - which has more
    weight for the project

## Notes
- It is important to create packages that represent the domains described
- ~~ We prefer 'parser' instead of 'parsable' to define the interfaces. Same
    that is less object-oriented, a parsable would have little sense in itself
    only. The resolution of a string to an expression is best defined by a
    third-party entity - a service. ~~
- Given the selected algorithm, parsable seemed more sensible 