package web

import (
	"fmt"
	"unicode/utf8"
)

type ArithmeticRequest struct {
	Expression string    `json:"expression"`
	Variables  variables `json:"variables"`
	Notation   string    `json:"notation"`
}

type variables map[string]float64

func (vars variables) convertToContext() (map[rune]float64, error) {
	val := map[rune]float64{}
	for index, varVal := range vars {
		if len(index) > 1 {
			return nil, fmt.Errorf("Variables must be single char only - %v received", index)
		}

		symbol, _ := utf8.DecodeRuneInString(index)
		val[symbol] = varVal
	}

	return val, nil
}

type ArithmeticResponse struct {
	Result float64 `json:"result"`
	Error  string  `json:"error"`
}

type arithmeticService interface {
	solve(expr string) (float64, error)
}

type arithmeticContext interface {
	setContext(ctx map[rune]float64)
}
