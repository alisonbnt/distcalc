package web

import (
	"issiata.com/dev/distcalc/arithmetic"
	"issiata.com/dev/distcalc/exprparse"
)

type WiredArithmeticService struct {
	parser exprparse.StrExprParser
}

func NewWiredArithmeticService(parser exprparse.StrExprParser) *WiredArithmeticService {
	return &WiredArithmeticService{
		parser: parser,
	}
}

func (service *WiredArithmeticService) solve(expr string) (float64, error) {
	expression, err := service.parser.Parse(expr)
	if err != nil {
		return 0.0, err
	}

	result, solveErr := expression.Solve()
	if solveErr != nil {
		return 0.0, solveErr
	}

	return result, nil
}

type WiredArithmeticContext struct {
	context *arithmetic.ArithmeticContext
}

func NewWiredArithmeticContext(context *arithmetic.ArithmeticContext) *WiredArithmeticContext {
	return &WiredArithmeticContext{
		context: context,
	}
}

func (context *WiredArithmeticContext) setContext(ctx map[rune]float64) {
	context.context.SetContext(arithmetic.ExprCtx(ctx))
}
