package web

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

type NotationPublisher interface {
	SetNotation(notation string)
}

type HTTPServer struct {
	addr   string
	notPub NotationPublisher
	arSrvc arithmeticService
	arCtx  arithmeticContext
}

func NewHTTPServer(addr string, notPub NotationPublisher, arSrvc arithmeticService, arCtx arithmeticContext) *HTTPServer {
	return &HTTPServer{
		notPub: notPub,
		addr:   addr,
		arSrvc: arSrvc,
		arCtx:  arCtx,
	}
}

func (server *HTTPServer) Run() {
	r := mux.NewRouter()
	r.HandleFunc("/", server.arithmeticHandler).Methods("POST")
	fmt.Printf("Server started - listening at %v\n", server.addr)
	http.ListenAndServe(server.addr, r)
}

func (server *HTTPServer) arithmeticHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var arReq ArithmeticRequest
	decodeErr := json.NewDecoder(r.Body).Decode(&arReq)
	if decodeErr != nil {
		server.sendArithmeticError(decodeErr, w, r)
		return
	}
	server.notPub.SetNotation(arReq.Notation)
	reqCtx, convErr := arReq.Variables.convertToContext()
	if convErr != nil {
		server.sendArithmeticError(convErr, w, r)
		return
	}
	server.arCtx.setContext(reqCtx)
	result, err := server.arSrvc.solve(arReq.Expression)

	if err != nil {
		server.sendArithmeticError(err, w, r)
		return
	}

	response := ArithmeticResponse{Result: result}
	encodeErr := json.NewEncoder(w).Encode(&response)
	server.checkEncodeError(encodeErr)
}

func (server *HTTPServer) sendArithmeticError(err error, w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusBadRequest)
	// TODO: Maybe it's a bad idea to expose internal error messages through HTTP
	res := ArithmeticResponse{Error: err.Error()}
	encodeErr := json.NewEncoder(w).Encode(&res)
	server.checkEncodeError(encodeErr)
	return
}

func (server *HTTPServer) checkEncodeError(err error) {
	if err != nil {
		// TODO: Panic in a HTTP context can be very complicated. Maybe trigger an event?
		panic(err)
	}
}
