DISTCALC
========

Se tem algo que é uma verdadeira prova de inteligência é a realização de
cálculos. Somar, subtrair, multiplicar e dividir são operações básicas, mas que
conseguiram mudar o rumo da humanidade. Agora, imagine conseguir realizar
operações matemáticas de forma distribuída? Esse projeto é pra isso!

## Metodologia da solução
Temos dois domínios a resolver nessa etapa do projeto:
- Resolução das expressões matemáticas
- Parsing de expressões matemáticas (depende do anterior)

Fora isso existem domínios de suporte à solução:
- Comunicação baseada em gRPC
- Servidor HTTP com um único endpoint (e leitura dos parâmetros do request)
- Service Discovery e Load Balacing dos serviços

A abordagem selecionada é resolver os domínios principais usando práticas de
Orientação a Objetos e em seguida implementar os serviços de modo a utilizar os
domínios de suporte. Desse modo conseguimos garantir a corretude do serviço e em
seguida sua acessibilidade e escalabilidade.

## Primeiro passo - Atacando expressões
1. Gerar casos de testes que representem expressões em formato de objetos e
   verificar se resolvem corretamente
2. Implementar objetos dos testes
3. Expandir testes com expressões mais complexas
4. Realizar implementações referentes aos novos testes


## Segundo passo - Parsing de strings
1. Esse pacote deve ter uma interface master (string) => solvable
2. O tipo solvable e o tipo expression são equivalentes (mas em pacotes
   diferentes)
3. Implementar três parsers, um para cada tipo de notação. Começar com a notação
   convêncional
4. Criar testes com expressões simples e partir para expressões mais complexas

## Terceiro passo - Endpoint HTTP
1. Resolvemos utilizar o mux para roteamento, dado que temos planos de expandir
   o serviço http futuramente

Nesse momento algumas decisões de projeto foram tomadas
- Optamos por realizar o parsing de expressões em string para tokens usando o
    algoritmo Shunting-yard
- A ideia inicial era converter a string diretamente para `arithmetic.Expression`,
    porém isso gerou uma forte dependência entre os pacotes. Optamos por criar
    um passo médio, onde convertemos a string em uma fila de tokens para,
    futuramente, converter a fila de tokens para `arithmetic.Expression`. Isso
    aumenta a complexidade geral do algoritmo, mas permite mais estabilidade -
    podemos testar o pacote `exprparse` individualmente
- Optamos por completar os testes de erros e caminhos alternativos futuramente,
    dado que ainda é necessário criar a estrutura distribuída - que tem mais
    peso para o projeto

## Notas
- Achamos importante criar pacotes que representem os domínios descritos
- ~~Preferimos 'parser' ao invés de 'parsable' para definir as interfaces. Mesmo
    que seja menos orientado a objetos, um parsable teria pouco sentido por si
    só. A resolução de uma string para uma expressão é melhor definida por um
    terceiro - um serviço.~~
- Dado o algoritmo selecionado, parsable pareceu mais sensato
