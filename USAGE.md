# USAGE

## POST /
Content-Type: application/json

Request samples:

### Conventional expression
```json
{"expression": "1 + 1"}
```
```json
{"result":2,"error":""}
```

### Variables
```json
{"expression": "x - 1", "variables": {"x" : 1}}
```
```json
{"result":0,"error":""}
```

### Parenthesis
```json
{"expression": "(2 + 2) * (3 - 1)"}
```
```json
{"result":8,"error":""}
```

### Convetional notation
```json
{"expression": "1 + 2 - x", "variables": {"x": 4}, "notation": "conventional"}
```
```json
{"result":-1,"error":""}
```

### Polish notation
```json
{"expression": "- + 1 2 x", "variables": {"x": 4}, "notation": "polish"}
```
```json
{"result":-1,"error":""}
```

### Reverse polish notation
```json
{"expression": "1 2 + x -", "variables": {"x": 4}, "notation": "reverse-polish"}
```
```json
{"result":-1,"error":""}
```

 